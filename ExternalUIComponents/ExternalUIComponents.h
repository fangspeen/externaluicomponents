//
//  ExternalUIComponents.h
//  ExternalUIComponents
//
//  Created by Jean Blignaut on 2016/07/19.
//  Copyright © 2016 Jean Blignaut. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ExternalUIComponents.
FOUNDATION_EXPORT double ExternalUIComponentsVersionNumber;

//! Project version string for ExternalUIComponents.
FOUNDATION_EXPORT const unsigned char ExternalUIComponentsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ExternalUIComponents/PublicHeader.h>


